#
#
# Name: LT Bot
# Author: Leonard Yeung & Tommy Chan
# invite: https://discordapp.com/api/oauth2/authorize?client_id=604668533779070976&permissions=8&scope=bot
#
#

f = open("Token.txt", "r")
TOKEN = f.readline()
TOKEN = TOKEN.rstrip("\n")
f.close()

import urllib.request as req
import bs4
import discord
import asyncio
from os import system
from urllib.parse import quote
import requests
from requests import get
import random
import json
import platform



if platform.system() == 'Windows':
	system("title LT Bot") #change title






timer = -1











def isInt(x):
	try:
		y = int(x)
	except:
		return False
	else:
		return True



def convertTuple(tup): 
    str =  ''.join(tup) 
    return str



def say(msg):
	return '```yaml\n'+msg+'```' #green text



def translate(msg, lang):
	if msg=='' or msg==None:
		return "What do you want to translate?"
	
	
	#msg=quote(msg)
	
	url="http://translate.googleapis.com/translate_a/single"
	
	headers={
		"User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
	}
	
	query = {
		"client": "gtx",
		"sl": 'auto',
		"tl": lang,
		"dt": "t",
		"q": msg,
	}
	
	result = requests.get(url, params=query, timeout=40, headers=headers, verify=False).text
	
	if result == '[,,""]':
		return "No result"
	
	while ',,' in result:
		result = result.replace(',,', ',null,')
		result = result.replace('[,', '[null,')
	
	data = json.loads(result)
	
	
	try:
		language = data[2]  # -2][0][0]
	except:
		language = '?'
	
	re=''.join(x[0] for x in data[0]), language
	
	re=convertTuple(re)[:-2]
	
	if lang=='en':
		re=re[:-3]
	
	return re


def googleSearch(msg):
	url=msg
	temp=url.split(' ', 1) #e.g. url=="1 2 3", temp[0]=="1", temp[1]=="2 3"

	if len(temp)<=1:
		return say("It will need a parameter(s). e.g. ..gg some words")
	try:
		url=temp[1]
		url=quote(url)
		url="https://www.google.com.hk/search?q="+url
		#print(url)
		request=req.Request(url, headers={
			"User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.72"
		})
		with req.urlopen(request) as response:
			data=response.read().decode("utf-8")


		root=bs4.BeautifulSoup(data, "html.parser")
		ans2=root.find_all("div", class_="r") #link of results

		#for ans in ans:
		#	print(ans.string)

		re=""
		for i in range(len(ans2)):
			#get the title
			re+="**`"+ans2[i].h3.string+"`**\n"
			#get the link
			re+="<"+ans2[i].a['href']+">\n\n"
	except UnicodeEncodeError:
		return say("Not supported character(s) are found.")
	else:
		#no UnicodeEncodeError
		if re=="":
			#no result found
			return say("No result.")
		else:
			return re

async def reminddetail(msg,currentchannelinfo):	
	if len(msg)<3:
		return "Not enough parameter."
	
	minutes_wait=msg[1]
	message_wait=''
	for i in range(2, len(msg)):
		message_wait+=' '+msg[i]
	message_wait=message_wait[1:] #to remove the first space character
	
	
	
	try:
		new_minute = int(minutes_wait)
	except:
		return "(Min) must be a number!"
	else:
		if message_wait == '':
			return "Please specify what I need to remind you!"
		else:
			await currentchannelinfo.channel.send("Got you! Will remind you in " + minutes_wait + " minute(s)!")
			await asyncio.sleep(new_minute * 60)
			return message_wait



def helpCommand():
	class help:
		def __init__(self, name, description):
			self.name = name
			self.description = description
	embed = discord.Embed(
		title = '-----Command list-----',
		colour = discord.Colour.blue()
	)
	commandList=[]
	commandList.append(help('..help', 'Command list.'))
	commandList.append(help('..gg (sth)', 'Google search sth.'))
	commandList.append(help('..remind (min) (message)', 'Bot will send this specific message after certain minutes.'))
	commandList.append(help('..live', 'Generate a link for live channel.'))
	commandList.append(help('..development', 'See our source code on GitLab and how you can submit your feedback.'))
	commandList.append(help('..chi (sth)', 'To translate sth to Chinese.'))
	commandList.append(help('..eng (sth)', 'To translate sth to English.'))
	commandList.append(help('..timer (set/start) (x)', 'To set a x minute(s) timer or start a timer(with 5 minutes and 15 minutes remind).'))
	commandList.append(help('..money', 'Check how much money you have now.'))
	commandList.append(help('..give (@user) (x)', 'To give him/her x dollars.'))
	commandList.append(help('..coinflip (head/tail) (bet amount)', 'Play a conflip game to earn some money... or lose it.'))
	
	for com in commandList:
		embed.add_field(name=com.name, value=com.description, inline=False)
	return embed

def check_user_exist(user_id):
	f = open('client.txt', "r")
	read_info = []
	temp = 0
	for i in f:
		read_info.append(i.rstrip('\n'))
		temp += 1
	f.close()
	for i in range(0,len(read_info),2):
		if read_info[i] == str(user_id):
			return True
	return False

def newplayer(user_id):
	f = open('client.txt', "a")
	f.write(str(user_id) + '\n100\n')
	f.close()

def coinflip(chosen_side,user_id,bet):
	f = open('client.txt', "r")
	read_info = []
	temp = 0
	for i in f:
		read_info.append(i.rstrip('\n'))
		temp += 1
	f.close()
	for i in range(0,len(read_info),2):
		if read_info[i] == str(user_id):
			break
	if int(read_info[i+1]) < bet:
		return "Not enough money in your account to bet!"
	result = random.randint(0,1)
	a = []
	a.append('head')
	a.append('tail')
	if a[result] == chosen_side:
		read_info[i + 1] = int(read_info[i+1]) + (bet * 2)
		return_message = 'You win! You now have ' + str(read_info[i+1]) + ' dollars in your account.'
	else:
		read_info[i + 1] = int(read_info[i + 1]) - bet
		return_message = 'You lose! You now have ' + str(read_info[i+1]) + ' dollars in your account.'
	f = open('client.txt', "w")
	for i in read_info:
		f.write(str(i)+'\n')
	f.close()
	return return_message

def give(user_id, temp, amount):
	if len(temp)==22:
		user_id2=temp[3:-1]
	elif len(temp)==21:
		user_id2=temp[2:-1]
	else: #not valid
		return "User not found."

	if user_id2=="":
		return "User not found."

	if not check_user_exist(int(user_id2)):
		return "User not found."

	if amount<=0:
		return "Amount must be greater than 0."

	f=open("client.txt", "r")
	read_info=[]
	for i in f:
		read_info.append(i.rstrip('\n'))
	f.close()
	for i in range(0,len(read_info),2):
		if read_info[i] == user_id:
			if int(read_info[i+1])<amount:
				return "You don't have enough money."
			read_info[i+1]=int(read_info[i+1])-amount
			break
	for i in range(0,len(read_info),2):
		if read_info[i] == user_id2:
			read_info[i+1]=int(read_info[i+1])+amount
			break
	f=open("client.txt", "w")
	for i in read_info:
		f.write(str(i)+'\n')
	f.close()
	return "Success!"

def gitdevelopment_details():
	message = []
	message.append("Follow our development on GitLab!")
	message.append("<https://gitlab.com/Leonard0503/lt_bot_project>")
	message.append("\n")
	message.append("Submit your feedback with the link below")
	message.append("<https://gitlab.com/Leonard0503/lt_bot_project/issues>")
	re = ""
	for com in message:
		re += com + "\n"
	return re


async def startTimer(channel):
	if timer < 5:
		await asyncio.sleep(timer * 60)
		await channel.send(say("Time's up!"))
	elif timer >= 5 and timer < 15:
		await asyncio.sleep((timer - 5) * 60)
		await channel.send(say("5 minutes left."))
		await asyncio.sleep(5 * 60)
		await channel.send(say("Time's up!"))
	else:
		await asyncio.sleep((timer - 15) * 60)
		await channel.send(say("15 minutes left."))
		await asyncio.sleep(10 * 60)
		await channel.send(say("5 minutes left."))
		await asyncio.sleep(5 * 60)
		await channel.send(say("Time's up!"))





class MyClient(discord.Client):
	async def on_ready(self):
		await client.change_presence(activity=discord.Game(name='..help'))
		print('Logged on as {0}!'.format(self.user))

	async def on_message(self, message):
		global timer
		if message.author == self.user:
			return #do not respond to self
		
		com = [] #init com
		msg = message.content
		com = ' '.join(msg.split()).split(' ') #e.g. msg="..gg test hi", com[0]=="..gg", com[1]=="test", com[2]=="hi"
		
		#print(com)
		
		#print(msg)
		if com[0]=="..help":
			await message.channel.send(embed=helpCommand())
		elif com[0]=="..gg":
			#send google search content
			await message.channel.send(googleSearch(msg))
		elif com[0]=="..remind":
			#Remind people after certain times base on minutes
			await message.channel.send('<@!'+str(message.author.id)+'> ' + await reminddetail(com,message))
		elif com[0]=="..timer":
			if len(com)<=1:
				await message.channel.send(say("Usage:\n..timer set (min)\n..timer start"))
			else:
				if com[1]=="set":
					if len(com) <= 2:
						await message.channel.send(say("Usage:\n..timer set (min)\n..timer start"))
					else:
						if isInt(com[2]):
							timer = int(com[2])
							await message.channel.send(say("Done!"))
						else:
							await message.channel.send(say("Minute(s) must be integer."))
				elif com[1]=="start":
					if timer == -1:
						await message.channel.send(say("Please use ..timer set (min) to set the timer first."))
					else:
						await message.channel.send(say("Start!\n" + str(timer) + " minute(s) left."))
						await startTimer(message.channel)
				else:
					await message.channel.send(say("Usage:\n..timer set (min)\n..timer start"))
		elif com[0]=="..live":
			if message.author.voice==None:
				await message.channel.send(say("Please join a voice channel."))
			else:
				await message.channel.send('Link:\n<https://discordapp.com/channels/'+str(message.guild.id)+'/'+str(message.author.voice.channel.id)+'>')
		elif com[0]=="..development":
			await message.channel.send(gitdevelopment_details())
		elif com[0]=="..money":
			if not check_user_exist(message.author.id):
				newplayer(message.author.id)
				await message.channel.send(say('You now have 100 dollars in your account.'))
			else:
				f=open("client.txt", "r")
				read_info=[]
				for i in f:
					read_info.append(i.rstrip('\n'))
				f.close()
				for i in range(0,len(read_info),2):
					if read_info[i] == str(message.author.id):
						await message.channel.send(say('You now have '+str(read_info[i+1])+' dollars in your account.'))
		elif com[0]=="..give":
			if len(com) == 1:
				await message.channel.send(say('Please tag the user that you want to give money to.'))
			elif len(com) == 2:
				await message.channel.send(say('Please specify the amount of money you want to give.'))
			else:
				try:
					amount = int(com[2])
				except ValueError or com[2]!="0":
					await message.channel.send(say('Amount to give must be integer!'))
				else:
					await message.channel.send(say(give(str(message.author.id), str(com[1]), amount)))
		elif com[0]=="..coinflip":
			if len(com) == 1:
				await message.channel.send(say('Please specify a side to bet on!'))
			elif len(com) == 2:
				await message.channel.send(say('Please specify amount of money to bet!'))
			else:
				try:
					bet_amount = int(com[2])
				except ValueError:
					await message.channel.send(say('Bet amount must be integer!'))
				else:
					com[1] = com[1].lower()
					if com[1] != 'head' and com[1] != 'tail':
						await message.channel.send(say('Only side "head" or "tail" can be chosen!'))
					elif int(com[2]) <= 0:
						await message.channel.send(say('Bet amount must be greater than 0!'))
					else:
						if not check_user_exist(message.author.id):
							newplayer(message.author.id)
						await message.channel.send(say(coinflip(com[1],message.author.id,bet_amount)))
		elif com[0]=="..chi":
			if len(com) == 1:
				await message.channel.send(say('What do you want to translate?'))
			else:
				temp=''
				for i in range(1, len(com)):
					temp+=com[i]+' '
				temp=temp[:-1] #To remove the last space character
				
				await message.channel.send(say(translate(temp, 'zh-TW')))
		elif com[0]=="..eng":
			if len(com) == 1:
				await message.channel.send(say('What do you want to translate?'))
			else:
				temp=''
				for i in range(1, len(com)):
					temp+=com[i]+' '
				temp=temp[:-1] #To remove the last space character
				
				await message.channel.send(say(translate(temp, 'en')))
client = MyClient()
client.run(TOKEN)
