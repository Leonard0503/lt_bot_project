appName="LTBot"

rm $appName

pyinstaller -F main.py --onefile --name $appName

cp "./dist/"$appName $appName

rm -r "dist"
rm -r "build"
rm -r "__pycache__"

rm $appName".spec"
