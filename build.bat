@echo off
title build exe

:: delete old exe
del /f /s /q "LTBot.exe"

:: build exe
pyinstaller -F main.py --onefile --name LTBot

:: copy exe from /dist to root folder
copy /Y "dist" "%cd%"

:: delete cache files
del /f /s /q "__pycache__\*.*"
del /f /s /q "build\*.*"
del /f /s /q "dist\*.*"
del /f /s /q "*.spec"

:: delete chche folders
rmdir /Q /S "__pycache__"
rmdir /Q /S "build"
rmdir /Q /S "dist"