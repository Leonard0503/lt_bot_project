# **Thank you for using the LT_BOT!**

## **What is LT_BOT?**


LT_BOT is a discord bot that is written in Python with the help of different libraries, such as discord.py.
    
This bot is currently under active development. Although there are very little features currently.
We will keep implementing different features to satify all users!


## **What does it do?**


It includes some features that can potentially boost your productivity. Such as a built-in google search system.
Just type .gg followed by the things that you want to search afterward. It currently support English and Chinese search.
    
It has some entertainment system as well. For example, typing ..coinflip (head/tail) (bet) and hope for the best. If you win, you will get double the bet you have put!

For more infomation on different features, invite the bot to your server and get started by typing "..help"


## **Who are the amazing developers of this bot?**

#### **Developers are as follow:**

- **Leonard Yeung**
- **Tommy Chan**


We are both high school students that are studying in Hong Kong. We have collaborated to develope multiple projects.
But every single one of them have became inactive because we abandon the project.

With this project we hope we can keep learning new stuffs through continuous development, which benefits both of us and the users.


## **Please submit your feedback in the issue tab! We would love to gather some opinions.**
## Invite Link: [https://discordapp.com/api/oauth2/authorize?client_id=604668533779070976&permissions=8&scope=bot](https://discordapp.com/api/oauth2/authorize?client_id=604668533779070976&permissions=8&scope=bot)